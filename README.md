# Infrastructure Incubateur des territoires

## Create new project

On a new branch run: `sh generate_project.sh <Your project title>`.

You then retrieve your project file into `projects/your-project-title.tf`.

Make sure everything is fine, add specific configuration and submit a pull request.
