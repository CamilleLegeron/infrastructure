#/bin/bash

if [ -z "$1" ]
then
  echo "Please provide your project name"
  exit 1
fi

PROJECT_NAME=$1
LOWER_CASE_NAME=$(echo $PROJECT_NAME | sed 's/.*/\L&/')
SNAKE_CASE_NAME=$(echo $LOWER_CASE_NAME | sed 's/ /_/g')

PROJECT_PATH=projects/$SNAKE_CASE_NAME.tf

cat << EOF > "$PROJECT_PATH"
module "$SNAKE_CASE_NAME" {
  source       = "./generic"
  common       = local.common
  project_name = "$PROJECT_NAME"
}
EOF
