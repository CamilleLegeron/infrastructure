resource "kubernetes_namespace" "mysql_operator" {
  metadata {
    name = "mysql-operator"
  }
}

module "mysql_operator" {
  source        = "gitlab.com/vigigloo/tools-k8s-mysql/operator"
  version       = "0.0.1"
  namespace     = kubernetes_namespace.mysql_operator.metadata[0].name
  chart_name    = "operator"
  chart_version = "2.0.9"
}
