locals {
  hostname = "zammad.dev.${var.dns_zone_incubateur}"
}

module "namespace" {
  source            = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version           = "1.0.1"
  max_cpu_requests  = "8"
  max_memory_limits = "16Gi"
  namespace         = "tools-zammad"
  project_name      = "Zammad"
  project_slug      = "zammad"

  default_container_cpu_requests  = "200m"
  default_container_memory_limits = "128Mi"
}

module "postgresql" {
  source     = "gitlab.com/vigigloo/tools-k8s/postgresql"
  version    = "0.1.0"
  chart_name = "postgresql"
  namespace  = module.namespace.namespace
}

module "elasticsearch" {
  source           = "gitlab.com/vigigloo/tools-k8s/elasticsearch"
  version          = "0.2.0"
  chart_name       = "elastic"
  namespace        = module.namespace.namespace
  image_repository = "registry.gitlab.com/vigigloo/tools/elasticsearch"
  image_tag        = "7.17.1-ingest-attachment"

  elasticsearch_replicas         = 2
  elasticsearch_persistence_size = "30Gi"
}

resource "random_password" "redis_password" {
  length  = 32
  special = false
}

module "redis" {
  source         = "gitlab.com/vigigloo/tools-k8s/redis"
  version        = "0.1.1"
  chart_name     = "redis"
  namespace      = module.namespace.namespace
  redis_password = resource.random_password.redis_password.result
  redis_replicas = 0
}

module "memcached" {
  source     = "gitlab.com/vigigloo/tools-k8s/memcached"
  version    = "0.1.1"
  chart_name = "memcached"
  namespace  = module.namespace.namespace
}

module "zammad" {
  source     = "gitlab.com/vigigloo/tools-k8s/zammad"
  version    = "0.1.1"
  chart_name = "zammad"
  namespace  = module.namespace.namespace
  values = [
    <<-EOT
    ingress:
      enabled: true
      annotations:
        acme.cert-manager.io/http01-edit-in-place: "true"
        cert-manager.io/cluster-issuer: letsencrypt-prod
        kubernetes.io/ingress.class: haproxy
      hosts:
        - host: ${local.hostname}
          paths:
            - path: /
              pathType: Prefix
      tls:
        - hosts:
            - ${local.hostname}
          secretName: zammad-tls
    EOT
  ]

  zammad_persistence_size = "30Gi"
  redis_host              = module.redis.hostname
  redis_password          = resource.random_password.redis_password.result
  memcached_host          = module.memcached.hostname
  elasticsearch_host      = module.elasticsearch.hostname
  postgresql_host         = module.postgresql.domain
  postgresql_user         = module.postgresql.superuser-username
  postgresql_password     = module.postgresql.superuser-password
  postgresql_database     = module.postgresql.database
}
