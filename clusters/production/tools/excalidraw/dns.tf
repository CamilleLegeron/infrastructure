resource "scaleway_domain_record" "excalidraw" {
  dns_zone = var.dns_zone_incubateur
  name     = "excalidraw"
  type     = "CNAME"
  data     = var.cluster_cname
  ttl      = 3600
}

locals {
  hostname = "${scaleway_domain_record.excalidraw.name}.${scaleway_domain_record.excalidraw.dns_zone}"
}
