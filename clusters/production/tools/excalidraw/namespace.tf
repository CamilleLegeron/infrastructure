module "namespace" {
  source            = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version           = "2.0.2"
  max_cpu_requests  = "3"
  max_memory_limits = "8Gi"
  namespace         = "excalidraw"
  project_name      = "Excalidraw"
  project_slug      = "excalidraw"
}
