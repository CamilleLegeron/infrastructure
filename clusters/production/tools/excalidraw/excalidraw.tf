module "excalidraw" {
  source        = "gitlab.com/vigigloo/tools-k8s/excalidraw"
  version       = "0.0.1"
  chart_name    = "excalidraw"
  chart_version = "0.1.1"
  depends_on    = [scaleway_domain_record.excalidraw]
  namespace     = module.namespace.namespace
  values = [
    <<-EOT

    ingress:
      enabled: true
      className: null
      annotations:
        acme.cert-manager.io/http01-edit-in-place: "true"
        cert-manager.io/cluster-issuer: letsencrypt-prod
        kubernetes.io/ingress.class: haproxy
      hosts:
        - host: ${local.hostname}
          paths:
            - path: "/"
              pathType: "Prefix"
      tls:
        - hosts:
            - ${local.hostname}
          secretName: excalidraw-tls
    resources:
      limits:
        memory: 256Mi
      requests:
        cpu: 50m
    EOT
  ]

}
