module "mattermost_namespace" {
  source            = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version           = "1.0.1"
  max_cpu_requests  = "6"
  max_memory_limits = "8Gi"
  namespace         = "mattermost"
  project_name      = "Mattermost"
  project_slug      = "mattermost"

  default_container_cpu_requests  = "200m"
  default_container_memory_limits = "128Mi"
}

module "mattermost" {
  source  = "gitlab.com/vigigloo/tools-k8s/mattermost"
  version = "0.1.0"

  chart_name    = "mattermost"
  chart_version = "6.3.1"
  namespace     = module.mattermost_namespace.namespace

  image_repository = "mattermost/mattermost-team-edition"
  image_tag        = "7.3"
  ingress_host     = local.hostname
  values = [
    file("${path.module}/mattermost.yaml")
  ]
  mattermost_externaldb                         = "postgres"
  mattermost_externaldb_connection_string       = var.mattermost_postgresql
  mattermost_configJSON_ServiceSettings_SiteURL = "https://${local.hostname}"

  limits_memory   = "2048Mi"
  requests_cpu    = "500m"
  requests_memory = "2048Mi"
}

moved {
  from = module.mattermost-namespace
  to   = module.mattermost_namespace
}
