variable "dns_zone_incubateur" {
  type = string
}
variable "cluster_cname" {
  type = string
}

variable "scaleway_access_key" {
  type = string
}
variable "scaleway_secret_key" {
  type      = string
  sensitive = true
}
variable "scaleway_project_id" {
  type = string
}
variable "scaleway_default_access_key" {
  type = string
}
variable "scaleway_default_secret_key" {
  type      = string
  sensitive = true
}
variable "scaleway_default_project_id" {
  type = string
}

variable "smtp_host" {
  type = string
}
variable "smtp_username" {
  type = string
}
variable "smtp_password" {
  type      = string
  sensitive = true
}
variable "smtp_from_email" {
  type = string
}

variable "gpg_key_email" {
  type = string
}
