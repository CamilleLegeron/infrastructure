module "namespace" {
  source            = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version           = "1.0.1"
  max_cpu_requests  = "9"
  max_memory_limits = "20Gi"
  namespace         = "directus"
  project_name      = "Directus"
  project_slug      = "directus"

  default_container_cpu_requests  = "200m"
  default_container_memory_limits = "128Mi"
}

resource "random_string" "directus_secret" {
  length  = 128
  special = false
  upper   = false
  lifecycle {
    ignore_changes = all
  }
}

resource "random_string" "directus_key" {
  length  = 128
  special = false
  upper   = false
  lifecycle {
    ignore_changes = all
  }
}

//Hack to store port
resource "random_string" "directus_database_port" {
  length = 5
  lifecycle {
    ignore_changes = all
  }
}

//Hack to store ip
resource "random_string" "directus_database_host" {
  length = 13
  lifecycle {
    ignore_changes = all
  }
}

resource "scaleway_object_bucket" "directus" {
  provider = scaleway.default-project
  name     = "incubateur-directus"
  region   = "fr-par"
}
resource "scaleway_object_bucket_acl" "directus" {
  provider = scaleway.default-project
  bucket   = scaleway_object_bucket.directus.name
  acl      = "public-read"
}

data "scaleway_rdb_instance" "directus" {
  provider    = scaleway.default-project
  instance_id = "7841c630-c9ff-44cd-9917-1085aa43d080"
}

data "scaleway_rdb_database" "directus" {
  provider    = scaleway.default-project
  instance_id = data.scaleway_rdb_instance.directus.instance_id
  name        = "directus"
}

resource "random_password" "directus_database_password" {
  length = 20
  lifecycle {
    ignore_changes = all
  }
}
module "directus" {
  source        = "gitlab.com/vigigloo/tools-k8s/directus"
  version       = "0.2.1"
  chart_name    = "directus"
  chart_version = "0.3.1"
  image_tag     = "9.7.1"
  namespace     = module.namespace.namespace
  ingress_host  = local.hostname
  values = [
    file("${path.module}/directus.yaml")
  ]
  directus_secret     = random_string.directus_secret.result
  directus_public_url = "https://${local.hostname}"
  directus_key        = random_string.directus_key.result

  directus_postgresql_host     = random_string.directus_database_host.result
  directus_postgresql_database = data.scaleway_rdb_database.directus.name
  directus_postgresql_username = data.scaleway_rdb_database.directus.name
  directus_postgresql_password = random_password.directus_database_password.result
  directus_postgresql_port     = random_string.directus_database_port.result

  directus_email_from          = "directus@incubateur.anct.gouv.fr"
  directus_email_smtp_host     = var.sendinblue_smtp_host
  directus_email_smtp_user     = var.sendinblue_smtp_user
  directus_email_smtp_password = var.sendinblue_smtp_password
  directus_email_smtp_port     = var.sendinblue_smtp_port
  directus_email_smtp_secure   = var.sendinblue_smtp_secure

  directus_storage_scaleway_bucket   = scaleway_object_bucket.directus.name
  directus_storage_scaleway_endpoint = "s3.fr-par.scw.cloud"
  directus_storage_scaleway_key      = var.scaleway_default_access_key
  directus_storage_scaleway_region   = "fr-par"
  directus_storage_scaleway_secret   = var.scaleway_default_secret_key

  directus_cors_enabled = true
  directus_cors_origin  = true
}

moved {
  from = random_string.directus-secret
  to   = random_string.directus_secret
}
moved {
  from = random_string.directus-key
  to   = random_string.directus_key
}
moved {
  from = random_password.directus-database-password
  to   = random_password.directus_database_password
}
