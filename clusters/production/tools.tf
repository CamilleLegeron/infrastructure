module "decidim" {
  source                      = "./tools/decidim"
  dns_zone_incubateur         = var.dns_zone_incubateur
  cluster_cname               = local.cluster_cname
  scaleway_access_key         = var.scaleway_access_key
  scaleway_secret_key         = var.scaleway_secret_key
  scaleway_default_access_key = var.scaleway_default_access_key
  scaleway_default_secret_key = var.scaleway_default_secret_key
  scaleway_default_project_id = var.scaleway_default_project_id
}

module "decidim_maires" {
  source              = "./tools/decidim_maires"
  scaleway_access_key = var.scaleway_access_key
  scaleway_secret_key = var.scaleway_secret_key
  scaleway_project_id = var.scaleway_project_id
}

module "decidim_inclusion_numerique" {
  source                      = "./tools/decidim_inclusion_num"
  scaleway_access_key         = var.scaleway_access_key
  scaleway_secret_key         = var.scaleway_secret_key
  scaleway_project_id         = var.scaleway_project_id
  scaleway_default_access_key = var.scaleway_default_access_key
  scaleway_default_secret_key = var.scaleway_default_secret_key
  scaleway_default_project_id = var.scaleway_default_project_id
  decidim_host                = "cnr-numerique.anct.gouv.fr"
  decidim_smtp_user           = var.decidim_inclusion_numerique_smtp_user
  decidim_smtp_password       = var.decidim_inclusion_numerique_smtp_password
}

module "directus" {
  source                      = "./tools/directus"
  dns_zone_incubateur         = var.dns_zone_incubateur
  cluster_cname               = local.cluster_cname
  scaleway_default_access_key = var.scaleway_default_access_key
  scaleway_default_secret_key = var.scaleway_default_secret_key
  scaleway_default_project_id = var.scaleway_default_project_id
  sendinblue_smtp_host        = var.sendinblue_smtp_host
  sendinblue_smtp_user        = var.sendinblue_smtp_user
  sendinblue_smtp_password    = var.sendinblue_smtp_password
  sendinblue_smtp_port        = var.sendinblue_smtp_port
  sendinblue_smtp_secure      = var.sendinblue_smtp_secure
}

module "mattermost" {
  source                = "./tools/mattermost"
  scaleway_access_key   = var.scaleway_default_access_key
  scaleway_secret_key   = var.scaleway_default_secret_key
  scaleway_project_id   = var.scaleway_default_project_id
  mattermost_postgresql = var.mattermost_postgresql
  dns_zone_incubateur   = var.dns_zone_incubateur
  cluster_cname         = local.cluster_cname
}

module "outline" {
  source                      = "./tools/outline"
  scaleway_default_access_key = var.scaleway_default_access_key
  scaleway_default_secret_key = var.scaleway_default_secret_key
  scaleway_default_project_id = var.scaleway_default_project_id
  mattermost_host             = module.mattermost.hostname
  outline_postgresql          = var.outline_postgresql
  outline_client_id           = var.outline_client_id
  outline_client_secret       = var.outline_client_secret
  dns_zone_incubateur         = var.dns_zone_incubateur
  cluster_cname               = local.cluster_cname
  outline_email_smtp_host     = var.outline_email_smtp_host
  outline_email_smtp_password = var.outline_email_smtp_password
  outline_email_smtp_user     = var.outline_email_smtp_user
}

module "grist" {
  source              = "./tools/grist"
  scaleway_access_key = var.scaleway_access_key
  scaleway_secret_key = var.scaleway_secret_key
  scaleway_project_id = var.scaleway_project_id
  dns_zone_incubateur = var.dns_zone_incubateur
  cluster_cname       = local.cluster_cname
  oauth_domain        = var.grist_oauth_domain
  oauth_client_id     = var.grist_oauth_client_id
  oauth_client_secret = var.grist_oauth_client_secret
  default_email       = var.grist_default_email

  scaleway_default_access_key = var.scaleway_default_access_key
  scaleway_default_secret_key = var.scaleway_default_secret_key
  scaleway_default_project_id = var.scaleway_default_project_id

  monitoring_org_id = var.grist_monitoring_org_id
}

resource "scaleway_domain_record" "matomo" {
  provider = scaleway.default-project
  dns_zone = var.dns_zone_incubateur
  name     = "matomo"
  type     = "CNAME"
  data     = local.cluster_cname
  ttl      = 3600
}
moved {
  from = module.matomo.scaleway_domain_record.matomo
  to   = scaleway_domain_record.matomo
}
module "matomo" {
  source              = "../../tools/matomo"
  scaleway_access_key = var.scaleway_access_key
  scaleway_secret_key = var.scaleway_secret_key
  backup_bucket       = "incubateur-matomo-backups-databases"
  hostname            = "${scaleway_domain_record.matomo.name}.${scaleway_domain_record.matomo.dns_zone}"
  providers = {
    scaleway.default-project = scaleway.default-project
    scaleway                 = scaleway
  }
}

module "passbolt" {
  source = "./tools/passbolt"

  dns_zone_incubateur = var.dns_zone_incubateur
  cluster_cname       = local.cluster_cname

  scaleway_access_key = var.scaleway_access_key
  scaleway_secret_key = var.scaleway_secret_key
  scaleway_project_id = var.scaleway_project_id

  scaleway_default_access_key = var.scaleway_default_access_key
  scaleway_default_secret_key = var.scaleway_default_secret_key
  scaleway_default_project_id = var.scaleway_default_project_id

  smtp_host       = var.sendinblue_smtp_host
  smtp_username   = var.sendinblue_smtp_user
  smtp_password   = var.passbolt_smtp_password
  smtp_from_email = var.sendinblue_smtp_user

  gpg_key_email = var.sendinblue_smtp_user
}

resource "scaleway_domain_record" "mobilizon" {
  provider = scaleway.default-project
  dns_zone = var.dns_zone_incubateur
  name     = "mobilizon"
  type     = "CNAME"
  data     = local.cluster_cname
  ttl      = 3600
}
module "mobilizon" {
  source = "../../tools/mobilizon"
  providers = {
    scaleway = scaleway
  }

  hostname             = "${scaleway_domain_record.mobilizon.name}.${scaleway_domain_record.mobilizon.dns_zone}"
  instance_name        = "Mobilizon Incubateur des Territoires"
  instance_description = "Instance Mobilizon déployée par l'Incubateur des Territoires."

  # used for backup to s3
  scaleway_secret_key   = var.scaleway_secret_key
  scaleway_access_key   = var.scaleway_access_key
  backup_bucket_db      = "incubateur-mobilizon-backups-databases"
  backup_bucket_uploads = "incubateur-mobilizon-backups-uploads"

  smtp_host        = var.sendinblue_smtp_host
  smtp_port        = var.sendinblue_smtp_port
  smtp_user        = var.sendinblue_smtp_user
  smtp_password    = var.sendinblue_smtp_password
  smtp_from_email  = var.sendinblue_smtp_user
  smtp_reply_email = var.sendinblue_smtp_user
}

module "kroki" {
  providers = {
    scaleway = scaleway.default-project
  }
  source              = "./tools/kroki"
  dns_zone_incubateur = var.dns_zone_incubateur
  cluster_cname       = local.cluster_cname
}

module "excalidraw" {
  providers = {
    scaleway = scaleway.default-project
  }
  source              = "./tools/excalidraw"
  dns_zone_incubateur = var.dns_zone_incubateur
  cluster_cname       = local.cluster_cname
}
