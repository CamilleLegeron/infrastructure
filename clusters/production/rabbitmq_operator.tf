resource "kubernetes_namespace" "rabbitmq" {
  metadata {
    name = "rabbitmq-operator"
  }
}

module "rabbitmq" {
  source  = "gitlab.com/vigigloo/tools-k8s-rabbitmq/operator"
  version = "0.0.1"

  namespace     = kubernetes_namespace.rabbitmq.metadata[0].name
  chart_name    = "rmqo"
  chart_version = "3.2.10"
}

