variable "dns_zone_incubateur" {
  type = string
}

variable "scaleway_project_id" {
  type = string
}

variable "scaleway_access_key" {
  type      = string
  sensitive = true
}

variable "scaleway_secret_key" {
  type      = string
  sensitive = true
}

variable "scaleway_default_project_id" {
  type = string
}

variable "scaleway_default_access_key" {
  type      = string
  sensitive = true
}

variable "scaleway_default_secret_key" {
  type      = string
  sensitive = true
}

variable "mattermost_postgresql" {
  type = string
}

variable "sendinblue_smtp_host" {
  type = string
}
variable "sendinblue_smtp_user" {
  type = string
}
variable "sendinblue_smtp_password" {
  type      = string
  sensitive = true
}
variable "sendinblue_smtp_port" {
  type = string
}
variable "sendinblue_smtp_secure" {
  type = string
}

variable "outline_postgresql" {
  type = string
}
variable "outline_client_id" {
  type = string
}
variable "outline_client_secret" {
  type = string
}

variable "outline_email_smtp_host" {
  type = string
}
variable "outline_email_smtp_user" {
  type = string
}
variable "outline_email_smtp_password" {
  type      = string
  sensitive = true
}

variable "decidim_inclusion_numerique_smtp_user" {
  type      = string
  sensitive = true
}
variable "decidim_inclusion_numerique_smtp_password" {
  type      = string
  sensitive = true
}

variable "grist_oauth_client_id" {
  type = string
}
variable "grist_oauth_domain" {
  type = string
}

variable "grist_oauth_client_secret" {
  type      = string
  sensitive = true
}

variable "grist_default_email" {
  type = string
}

variable "grist_monitoring_org_id" {
  type      = string
  sensitive = true
}

variable "passbolt_smtp_password" {
  type      = string
  sensitive = true
}
