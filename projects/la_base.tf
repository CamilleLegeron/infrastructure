module "la_base" {
  source               = "./generic"
  common               = local.common
  project_name         = "La Base"
  with_tf_project      = false
  with_scw_project     = true
  with_record          = false
  with_wildcard_record = false
  providers = {
    scaleway.iam = scaleway.iam
  }
}
