module "mon_stage_de_troisieme" {
  source       = "./generic"
  common       = local.common
  project_name = "Mon stage de troisieme"
  providers = {
    scaleway.iam = scaleway.iam
  }
}
