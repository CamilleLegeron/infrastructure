module "aidantsconnect" {
  source       = "./generic"
  common       = local.common
  project_name = "AidantsConnect"
  providers = {
    scaleway.iam = scaleway.iam
  }
}
