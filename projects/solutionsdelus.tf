resource "scaleway_domain_record" "solutionsdelus_validation" {
  dns_zone = var.dns_zone_incubateur
  name     = "_scaleway-challenge.solutionsdelus"
  type     = "TXT"
  data     = "ba2a4f0c-9c02-46aa-9b09-286e75b7489b"
}
