module "mediature" {
  source       = "./generic"
  common       = local.common
  project_name = "Mediature"
  providers = {
    scaleway.iam = scaleway.iam
  }
}
