terraform {
  required_providers {
    scaleway = {
      source = "scaleway/scaleway"
      configuration_aliases = [
        scaleway.iam,
      ]
    }
    grafana = {
      source = "grafana/grafana"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
    }
  }
}
