locals {
  scw_project_switch        = var.with_scw_project ? 1 : 0
  gl_scw_project_var_switch = var.with_scw_project && var.with_tf_project ? 1 : 0
  scw_managed               = "Managed by terraform"
  scw_admin                 = "${local.project_name} admin"
}

# Domain records

resource "scaleway_domain_record" "record" {
  count    = var.with_record ? 1 : 0
  dns_zone = var.common.dns_zone_incubateur
  name     = local.project_subdomain
  type     = "CNAME"
  data     = var.common.production_cluster_cname
  ttl      = 3600
}

resource "scaleway_domain_record" "record_wildcard" {
  count    = var.with_wildcard_record ? 1 : 0
  dns_zone = var.common.dns_zone_incubateur
  name     = "*.${local.project_subdomain}"
  type     = "CNAME"
  data     = var.common.production_cluster_cname
  ttl      = 3600
}

# Scaleway project
resource "scaleway_account_project" "project" {
  provider    = scaleway.iam
  count       = local.scw_project_switch
  name        = local.project_name
  description = local.scw_managed
}

output "scaleway_project" {
  value = var.with_scw_project ? scaleway_account_project.project[0].id : ""
}

# Scaleway admin application
resource "scaleway_iam_application" "project_admin" {
  provider    = scaleway.iam
  count       = local.scw_project_switch
  name        = local.scw_admin
  description = local.scw_managed
}
resource "scaleway_iam_group" "project_admin" {
  provider        = scaleway.iam
  count           = local.scw_project_switch
  name            = local.scw_admin
  description     = local.scw_managed
  application_ids = [scaleway_iam_application.project_admin[0].id]
}
resource "scaleway_iam_policy" "project_admin" {
  provider = scaleway.iam
  count    = local.scw_project_switch
  name     = local.scw_admin
  group_id = scaleway_iam_group.project_admin[0].id
  rule {
    project_ids          = [scaleway_account_project.project[0].id]
    permission_set_names = ["AllProductsFullAccess"]
  }
  description = local.scw_managed
}
resource "scaleway_iam_api_key" "admin_key" {
  provider           = scaleway.iam
  count              = local.scw_project_switch
  application_id     = scaleway_iam_application.project_admin[0].id
  description        = local.scw_managed
  default_project_id = scaleway_account_project.project[0].id
}

# Setting admin keys in infrastructure project
# Naming should be understood as "project id for the scaleway project" and "access key for the scaleway project"...
resource "gitlab_project_variable" "project_id" {
  count   = local.gl_scw_project_var_switch
  project = module.gitlab_project[0].project_id
  key     = "TF_VAR_scaleway_project_project_id"
  value   = scaleway_account_project.project[0].id
}
resource "gitlab_project_variable" "project_access_key" {
  count   = local.gl_scw_project_var_switch
  project = module.gitlab_project[0].project_id
  key     = "TF_VAR_scaleway_project_access_key"
  value   = scaleway_iam_api_key.admin_key[0].access_key
}
resource "gitlab_project_variable" "project_secret_key" {
  count   = local.gl_scw_project_var_switch
  project = module.gitlab_project[0].project_id
  key     = "TF_VAR_scaleway_project_secret_key"
  value   = scaleway_iam_api_key.admin_key[0].secret_key
}

# Creating custom groups
resource "scaleway_iam_group" "custom_group" {
  for_each    = var.scw_custom_groups
  provider    = scaleway.iam
  name        = "${local.project_name} ${each.key}"
  description = local.scw_managed
  lifecycle {
    ignore_changes = [
      user_ids,
      application_ids,
    ]
  }
}
resource "scaleway_iam_policy" "custom_group" {
  for_each    = var.scw_custom_groups
  provider    = scaleway.iam
  name        = scaleway_iam_group.custom_group[each.key].name
  description = local.scw_managed
  group_id    = scaleway_iam_group.custom_group[each.key].id
  rule {
    project_ids          = [scaleway_account_project.project[0].id]
    permission_set_names = each.value
  }
}
