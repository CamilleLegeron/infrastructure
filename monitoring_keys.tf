resource "random_string" "org_id_shared_donnees_territoires_transverse" {
  special = false
  length  = 63
}

moved {
  from = random_string.org-id-shared-donnees-territoires-transverse
  to   = random_string.org_id_shared_donnees_territoires_transverse
}
