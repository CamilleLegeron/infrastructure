provider "scaleway" {
  alias           = "default_project"
  region          = "fr-par"
  zone            = "fr-par-1"
  organization_id = var.scaleway_organization_id
  project_id      = var.scaleway_default_project_id
  access_key      = var.scaleway_default_access_key
  secret_key      = var.scaleway_default_secret_key
}

provider "scaleway" {
  alias           = "iam"
  region          = "fr-par"
  zone            = "fr-par-1"
  organization_id = var.scaleway_organization_id
  project_id      = var.scaleway_default_project_id
  access_key      = var.scaleway_iam_access_key
  secret_key      = var.scaleway_iam_secret_key
}

provider "grafana" {
  alias = "production"
  url   = module.cluster-production.grafana_url
  auth  = module.cluster-production.grafana_auth
}

provider "grafana" {
  alias = "development"
  url   = module.cluster-development.grafana_url
  auth  = module.cluster-development.grafana_auth
}
