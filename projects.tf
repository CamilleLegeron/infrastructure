module "projects" {
  source = "./projects"
  providers = {
    scaleway     = scaleway.default_project
    scaleway.iam = scaleway.iam
  }
  development_cluster_cname = module.cluster_development.cluster_cname
  dns_zone_incubateur       = local.dns_zone_incubateur
  production_cluster_cname  = module.cluster_production.cluster_cname
  development_base_domain   = var.development_base_domain
  production_base_domain    = var.production_base_domain

  scaleway_organization_id                = var.scaleway_organization_id
  scaleway_cluster_development_cluster_id = module.cluster_development.scaleway_cluster_id
  scaleway_cluster_production_cluster_id  = module.cluster_production.scaleway_cluster_id


  grafana_development_auth           = module.cluster_development.grafana_auth
  grafana_development_url            = module.cluster_development.grafana_url
  grafana_development_loki_url       = module.cluster_development.loki_url
  grafana_development_prometheus_url = module.cluster_development.prometheus_url

  grafana_production_auth           = module.cluster_production.grafana_auth
  grafana_production_url            = module.cluster_production.grafana_url
  grafana_production_loki_url       = module.cluster_production.loki_url
  grafana_production_prometheus_url = module.cluster_production.prometheus_url

  sit_vm_ip             = var.sit_vm_ip
  production_haproxy_ip = module.cluster_production.haproxy_ip
}
