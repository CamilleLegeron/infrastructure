resource "scaleway_object_bucket" "uploads_backups" {
  count = local.backups_enabled ? 1 : 0
  name  = var.backup_bucket_uploads
}

resource "kubernetes_cron_job" "backup" {
  count = local.backups_enabled ? 1 : 0
  metadata {
    name      = "backup-mobilizon-uploads"
    namespace = module.namespace.namespace
  }

  spec {
    concurrency_policy            = "Replace"
    failed_jobs_history_limit     = 10
    schedule                      = "25 3 * * *"
    starting_deadline_seconds     = 10
    successful_jobs_history_limit = 10

    job_template {
      metadata {
        annotations = {}
        labels      = {}
      }

      spec {
        backoff_limit = 3

        template {
          metadata {
            annotations = {}
            labels      = {}
          }
          spec {
            container {
              name  = "mobilizon-backup"
              image = "registry.gitlab.com/vigigloo/tools/mobilizon/backup"

              resources {
                limits = {
                  memory = "512Mi"
                }
                requests = {
                  cpu = "100m"
                }
              }

              env {
                name  = "S3_BUCKET"
                value = scaleway_object_bucket.uploads_backups[0].name
              }

              env {
                name  = "S3_REGION"
                value = scaleway_object_bucket.uploads_backups[0].region
              }

              env {
                name  = "S3_ENDPOINT_URL"
                value = scaleway_object_bucket.uploads_backups[0].endpoint
              }

              env {
                name  = "S3_ACCESS_KEY"
                value = var.scaleway_access_key
              }

              env {
                name  = "S3_SECRET_KEY"
                value = var.scaleway_secret_key
              }

              volume_mount {
                name       = "data"
                mount_path = "/data"
                read_only  = true
              }
            }

            affinity {
              pod_affinity {
                required_during_scheduling_ignored_during_execution {
                  label_selector {
                    match_expressions {
                      key      = "app.kubernetes.io/name"
                      operator = "In"
                      values   = ["mobilizon"]
                    }
                  }
                  topology_key = "kubernetes.io/hostname"
                }
              }
            }

            volume {
              name = "data"
              persistent_volume_claim {
                claim_name = "mobilizon-uploads"
              }
            }
          }
        }
      }
    }
  }
}
