terraform {
  required_providers {
    helm = {
      source = "hashicorp/helm"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
    }
    scaleway = {
      source = "scaleway/scaleway"
      configuration_aliases = [
        scaleway,
        # scaleway.default-project,
      ]

    }
    random = {
      source = "hashicorp/random"
    }
  }
  required_version = ">= 0.13"
}
