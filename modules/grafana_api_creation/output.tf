output "org_id" {
  value = grafana_organization.org.org_id
}

output "api_key" {
  value = grafana_api_key.org.key
}
