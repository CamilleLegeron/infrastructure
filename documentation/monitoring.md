# Monitoring

We're using a multi-tenant configuration to split views and access for teams.

Multi-tenant configuration is based on a secret key (a.k.a. Org ID) set by scrapers as a label, and passed in headers to frontends.

If a team requires a monitoring access, we should generate a secret key (recommandation to max 63 chars length), and add it to annotations, named `monitoring-org-id`.

Then, in organisation created for this team, we should add a datasource, with extra header, named `X-Scope-OrgID` with the same secret key.

